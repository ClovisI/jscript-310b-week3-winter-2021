// 1. Create a class function SpaceShip
// - should set two properties: name and topSpeed
// - should have a method accelerate that logs to the console
//   `${name} moving to ${topSpeed}`
function spaceShip(name, topSpeed){
  this.name=name;
  this.topSpeed=topSpeed;
}

// 2. Call the constructor with a couple ships,
// and call accelerate on both of them.
